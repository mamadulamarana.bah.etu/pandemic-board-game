# l2s4-projet-2023

Vous devez *forker* ce projet dans votre espace de travail Gitlab (bouton `Fork`) et vidéo sur le [portail](https://www.fil.univ-lille.fr/portail/index.php?dipl=L&sem=S4&ue=Projet&label=Documents)
Un unique fork doit être réalisé par équipe.

Une fois cela réalisé, supprimer ces premières lignes et remplissez les noms des membres de votre équipe.
N'oubliez pas d'ajouter les autres membres de votre équipe aux membres du projet, ainsi que votre enseignant·e (statut Maintainer).

# Equipe

- Rayane Alli
- Mamadu-Lamarana Bah
- Mathéis Buffet
- Aloïs Legrand

# Sujet

[Le sujet 2023](https://www.fil.univ-lille.fr/~varre/portail/l2s4-projet/sujet2023.pdf)

# Livrables

## Livrable 1  
déposé (pas d'erreurs).
### Atteinte des objectifs
Creation de la map et des villes => affichage a l'aide de println lors de l'execution du Livrable1.

### Difficultés restant à résoudre
Aucune
## Livrable 2

### Atteinte des objectifs
Creation des paquets de cartes, des joeurs, maladies et des défausses
### Difficultés restant à résoudre
Aucune
## Livrable 3

### Atteinte des objectifs
creation des Actions et corrections de codes dans des classes du precedent livrable
### Difficultés restant à résoudre
pouvoir integrer facilement des actions en extension sans
## Livrable 4

### Atteinte des objectifs

### Difficultés restant à résoudre

# Journal de bord

## Semaine 1
creation des UMLs et debut de code de la classe Map
## Semaine 2
Creation d'une classe pour l'Extraction des données d'un fichier JSON.
## Semaine 3
Correction d'erreurs et de bugs sur les classes Mappe, ClassicalMappe, JSONReader.
## Semaine 4
Rendu Livrable1.
## Semaine 5
creation des decks (paquets de cartes player epidemic et infection)
## Semaine 6
création de la défausse pour les différentes cartes 
## Semaine 7
création des roles de joueur
## Semaine 8
corrections de bugs et finalisation des roles de joueurs
## Semaine 9
implantation des actions et mise à jour de quelques classes
## Semaine 10

## Semaine 11

## Semaine 12
