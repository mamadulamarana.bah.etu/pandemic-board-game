import java.util.*;


/** util pour pas déranger le game du livrable2 qui marche */

import carte.*;
import carte.deck.*;
import util.io.Input;
import map.*;
import Player.*;
import Player.role.Role;
import Player.role.StationAvailableException;
import town.*;
import util.listchooser.*;
import virus.*;
/**
 * 
 * @author mamad
 * class of the Game
 */
public class Game {
	private static final Random RAND = new Random();

	private static final int NB_PLAYER_LIMIT = 4;

	private static final int NB_ACTION = 4;
	private static final int NB_REMEDY = 4;
	private static final int NB_FOYERS = 8;

	private static final int NB_TAKED_PLAYER_CARD = 2;

	
	private Mappe map;
	private Deck deckPlayerCard;
	private Deck deckInfectionCard;
	private List<Player> players;
	private int infectionLevel;
	private boolean epidemie;
	private boolean finish;
	private Iterator<Player> it; /** (global variable) an iterator for to browse the players */
	
	private List<String> choice = new ArrayList<>();
	private List<Role> roles;
	
	/**
	 * build an instance of game and start playing the players
	 * @param map the map of the game with all towns
	 */
	public Game(Mappe map) {
		this.map = map;
		this.deckPlayerCard = this.map.getDeckPlayer();
		this.deckInfectionCard = this.map.getDeckInfection();
		this.players = new ArrayList<>();
		this.infectionLevel = 2;
		this.finish = false;
		this.epidemie = false;
		this.roles = new ArrayList<>();
		
		choice.add("oui");
		choice.add("non");
	}
	
	/**
	 * get the deck of playerCard
	 * @return deck of playerCard
	 */
	public Deck getPlayerCard() {
		return this.deckPlayerCard;
	}
	
	/**
	 * get the deck of infectionCard
	 * 
	 * @return deck of infectionCard
	 */
	public Deck getInfectionCard() {
		return this.deckInfectionCard;
	}
	
	/**
	 * get the PlayerCard Discard
	 *
	 * @return the Player Discard
	 */
	public Discard getPlayerCardDiscard() {
		return this.map.getNormalDiscard();
	}
	
	/**
	 * get the infectionCard discard
	 * @return the infection discard
	 */
	public Discard getInfectionCardDiscard() {
		return this.map.getInfectionDiscard();
	}

	/**
	 * the different roles in the game
	 * @param roles list of the roles (for the choice of players)
	 */
	public void setRoles(List<Role> roles) {
		this.roles.addAll(roles);
	}

	/**
	 * add a player in the game.
	 * @param p added Player
	 */
	public void addPlayer(Player p) {
			if (this.players.size() < Game.NB_PLAYER_LIMIT) {
				if ( !(this.players.contains(p)) ) {
					p.setRole(this.roles);
					this.players.add(p);
				}
			}
	}
		
	/**
	 * get the number of Players in the game
	 * @return the number of Players
	 * 
	 */
	public int getNbPlayers() {
		return this.players.size();
	}
	
	/**
     * get the players
     * 
     * @return the tray of players
     */
    public List<Player> getPlayers() {
        return this.players;
    }
	
	/**
     * the next player who plays
     * 
     * @return the player whose turn it is
     */
    public Player nextPlayer() {	/**modif UML */
        Player p;
        try {
            if (it.hasNext()) {
                p = it.next();
                return p;
            }
            else {
                it = this.players.iterator();
                p = it.next();
                return p;
            }
        } catch (NullPointerException e) {
            it = this.players.iterator();
            p = it.next();
            return p;
        }
    }
    
    /**
     * set the game is over
     * 
     */
    private void setgameFinished() {
        this.finish = true;
    }
    
    /**
     * get all the town on the map
     * @return all the town on the map
     */
    public List<Town> getTowns() {
    	return this.map.getTowns();
    }
    
    /**
     * determine if the game is finished or not
     * 
     * @return (true) if the game is finished and one player has won, (false) or not
     */
    public boolean isFinished() {
        return this.finish;
    }
    
    /**
     * check if the players are win or not
     * 
     * @return true if the players are win, else return false if the disease are win.
     */
    private boolean checkWinner() {
    	return Disease.getNbRemedy() == Game.NB_REMEDY;
    }
    
    /**
     * check if the number of outbreaks reaches NB_FOYERS.
     * 
     * @return true if the number of outbreaks reaches NB_FOYERS, false or not.
     */
    private boolean checkNbFoyers() {
    	int nbFoyers = Town.getNbFoyers();
    	System.out.println("le nombre de foyers d'infection est de "+nbFoyers);
    	
    	if (nbFoyers == Game.NB_FOYERS) {
    		this.setgameFinished();
    		return true;
    	}
    	return false;
    }
    
    /**
     * check if the remedies for the 4 diseases are discovered.
     * 
     * @return true if all the remedies are discovered, false or not.
     */
    private boolean checkNbRemedy() {
    	int nbRemedy = Disease.getNbRemedy();
    	System.out.println("le nombre de remede trouve est de "+nbRemedy);
    	if (nbRemedy == Game.NB_REMEDY) {
    		this.setgameFinished();
    		return true;
    	}
    	return false;
    }
    
    /**
     * increases the overall rate of infection
     * 
     */
    private void increaseInfectionLevel() {
    	this.infectionLevel++;
    }
    
    /**
     * an epidemic triggered by an epidemic card
     * 
     */
    private void setEpidemie() {
    	this.epidemie = true;
    }
    
    /**
     * Was there an epidemic that was triggered by an epidemic card?
     * 
     * @return true if there are an epidemic, false or not.
     */
    public boolean isEpidemie() {
    	return this.epidemie;
    }
    
    /**
     * manage an infection of a disease that has been eradicated
     * triggering a normal infection phase or following an epidemic
     * 
     */
    private void infectionPhase() {
    	try {
	    	if (!this.epidemie) {
	    		this.normalInfectionPhase();
	    	}
	    	else {
	    		this.epidemicInfectionPhase();
	    	}
	    	for (Town town : this.getTowns()) {
	    		if (town.isOutbreak()) {
	    			town.setOutbreak(false);
	    		}
	    	}
    	}catch(NotAvailableCubeException e) {
    		System.out.println(e.getMessage());
    		this.setgameFinished();
    	}
    }
    
    /**
     * Normal infection (pas epidemic)
     * @throws NotAvailableCubeException there are no more cubes available.
     * 
     */
    private void normalInfectionPhase() throws NotAvailableCubeException {
		System.out.println("phase d'infection normale (pas de carte epidemique)");
		System.out.println("le taux d'infection global est de "+this.infectionLevel);
		for (int i=0; i<this.infectionLevel; i++) {
    		Card infectionCard = this.getInfectionCard().pick();
    		Disease cardDisease = infectionCard.getDisease();
    		Town cardTown = infectionCard.getTown();
    		cardTown.infection(cardDisease, 1);
    		this.map.getInfectionDiscard().addCard(infectionCard);
    		System.out.println("cette carte infection a ete defausse (mis dans la defausse correspondante)");
		}
    }
    
    /**
     * the cards from the infection card discard pile are shuffled and placed back on top of the
     * pile of infection cards
     * 
     */
    private void replaceInfectionCard() {
    	List<Card> discardedCardInfection = this.getInfectionCardDiscard().getDiscard();
    	Collections.shuffle(discardedCardInfection);
    	this.getInfectionCard().getStack().addAll(discardedCardInfection);
	}

    /**
     * infection following the drawing of an epidemic card
     * @throws NotAvailableCubeException there are no more cubes available
     */
    private void epidemicInfectionPhase() throws NotAvailableCubeException {
		this.increaseInfectionLevel();

		System.out.println("phase d'infection suite au piochage d'une carte epidemique");
		System.out.println("le taux d'infection est passe a "+this.infectionLevel);

		this.epidemie = false;
		Card infectionCard = this.getInfectionCard().pick();
		Disease cardDisease = infectionCard.getDisease();
		Town cardTown = infectionCard.getTown();
		cardTown.infection(cardDisease, 1);
		this.replaceInfectionCard();
    }
    
	/**
     * initial placement of players in a city (random)
     */
    private void setPlayersOnTown() {
		ListChooser<Town> lc = new RandomListChooser<>();
		Town town = lc.choose("Dans quelle ville voulez-vous placez vos joueurs", this.map.getTowns());
		town.setStation();
    	System.out.println(" ils sont tous sur la "+town);
		this.displayTownStation(town);

		System.out.println("les joueurs de Pandemic sont :");
    	for (Player player: this.players) {
    		player.setTown(town);
			System.out.println(player +" "+player.getRole());
    	}
    }
    
    /**
     * display if there are a station in a given town or not.
     */
    private void displayTownStation(Town town) {
    	if (town.isStation()) {
        	System.out.println("Cette ville dispose d'une station de recherche");
    	}
    	else {
        	System.out.println("Cette ville ne dispose pas d'une station de recherche");
        	System.out.println();
    	}
    }
    
    /**
     * carry out the actions of a player
     * @param p a player
     */
    private void action(Player p) {
    	try {
    		while (p.getNbAction() < Game.NB_ACTION) {
    	    	this.displayTownSituation(p.getTown());
    	    	System.out.println("vous disposez de ces cartes : "+p.getMain());
    			p.play();
    		}
    		p.resetNbAction();
	    	this.displayTownSituation(p.getTown());
		}catch (PlayerCardUnavailableException | StationUnavailableException | StationAvailableException e) {
			System.out.println(e.getMessage());
			this.action(p);
		}
    }
    
    /**
     * displays the health situation of a World
     * 
     */
    private void displayInfectionInTowns() {
    	System.out.println();
    	ListChooser<String> ilc = new InteractiveListChooser<>();
    	String readChoice = ilc.choose("Souhaitez-vous un obtenir un Rapport sur la situation sanitaire du  monde", choice);
		if (readChoice.equals("oui")) {
			System.out.println("Situation Sanitaire des Villes apres les infections survenues");
	    	for (Town town : this.getTowns()) {
	    		System.out.println("la '"+town+"' est infecte par "+town.getNbDisease()+" maladie : "+town.getDiseases());
	    	}
		}
    }
    
    /**
     * displays the health situation of a city
     * @param town a given town
     */
    private void displayTownSituation(Town town) {
    	System.out.println("voici un Rapport sur la situation sanitaire de la " + town);
    	System.out.println("elle est infecte par :"+town.getNbDisease() + " maladie " + town.getDiseases());
    	this.displayTownStation(town);
    }
    
    /**
     * Main initiale des joueurs
     */
    private void initPlayerMain() {
    	Iterator<Player> it = this.players.iterator();
    	int nbCard = 0;
    	if (this.getNbPlayers() == 2) { nbCard = 4; }
    	else if (this.getNbPlayers() == 3) {nbCard = 3; }
    	else { nbCard = 2; }
    	
    	while (it.hasNext()) {
    		Player p = it.next();
    		for (int i = 0; i < nbCard; i++) {
    			p.addCard(this.getPlayerCard().pick(), this.map.getNormalDiscard());
    		}
    	}
    }
     
    /**
     * Initial infection
     */
    private void infectionInitiale() {
    	try {
	    	Iterator<Town> it = this.getTowns().iterator();
	    	ListIterator<Town> it_reverse = this.getTowns().listIterator(this.getTowns().size());
	    	for (int i = 0; i< 9; i++) {
	    		Card card = this.map.getDeckInfection().pick();
	    		if (it.hasNext() && i < 3) {
	    			it.next().infection(card.getDisease(), 3);
	    		}
	    		else if (it.hasNext() && i < 6) {
	    			it.next().infection(card.getDisease(), 2);
	    		}
	    		else {
	    			if (it_reverse.hasPrevious()) {
	    				it_reverse.previous().infection(card.getDisease(), 1);
	    			}
	    		}
				this.map.getInfectionDiscard().addCard(card);
	    	}
	    	this.displayInfectionInTowns();
    	}catch(NotAvailableCubeException e) {
    		System.out.println(e.getMessage());
    		this.setgameFinished();
    	}
    }
    
    /**
     * the player draws these 2 player cards
     * 
     */
    private void playerPickCard(Player p) {
    	for (int i =0; i < Game.NB_TAKED_PLAYER_CARD; i++) {
	    	Card card = p.takeCard(this.deckPlayerCard);
	    	
	    	if ( !(card.isEpidemic()) ) {
	        	System.out.println(p + " a pris "+card);
	    		p.addCard(card, this.map.getNormalDiscard());
	    	}
	    	else {
	    		System.out.println(p +" a pioche une carte epidemic");
	    		this.setEpidemie();
	    		this.infectionPhase();
	    		this.map.getEpidemicDiscard().addCard(card);
	    		System.out.println("la carte epidemic a ete defausse (mis dans la defausse correspondante)");
	    	}
    	}
    }
    
    /**
     *make a player and perform these actions
     *
     */
    public void playerPlay() {
    	System.out.println();
    	System.out.println("tapez 'next' pour faire jouer un joueur ou sinon tapez (display) pour avoir un rapport sur la situation sanitaire");
		String readString = Input.readString().toLowerCase();
    	if (readString.equals("next")) {
	    	Player p = this.nextPlayer();
	    	
	    	System.out.println(p +" "+p.getRole()+ " joue");
	    	this.action(p);
	    	try {
		    	System.out.println( p +" va piocher ces cartes joueurs");
		    	this.playerPickCard(p);
	    	}catch(EmptyStackException e) {
	    		System.out.println("plus de carte joueurs disponibles");
	    		this.setgameFinished();
	    	}
    	}
    	else if (readString.equals("display")) {
    		this.displayInfectionInTowns();
    		this.playerPlay();
    	}
    	else {
    	System.out.println("Saisie invalide");
    	this.playerPlay();
    	}
    }
    
    /**
     * In turn, a player must perform four of the possible actions. For some actions, he will use the
	 * player cards in his hand. Once these actions have been completed,
	 * he draws 2 player cards from the pile of player cards and adds them to his hand.
	 * add them to his hand. Then an infection phase is started (see below). We move on to the next player.
	 * 
     */
	public void play() {
		ListChooser<String> ilc = new InteractiveListChooser<>();
		String readChoice = ilc.choose("Voulez-vous jouer a Pandemic ", choice);
		if (readChoice.equals("oui")) {
			this.setPlayersOnTown();
			System.out.println("Ready...go save the world");
			this.initPlayerMain();
			this.deckPlayerCard.finalInitPlayerCard();
			this.infectionInitiale();
			
			while (!this.isFinished()) {
				this.playerPlay();
				boolean playerCard = this.getPlayerCard().getStack().size() == 0;
				if ( !this.checkNbRemedy() && !this.checkNbFoyers() && !playerCard) {
					this.infectionPhase();
				}
			}
			
			if (this.checkWinner()) {
				System.out.println();
				System.out.println("Bravo vous avez vaincu tous les remedes....Yesssss");
			}
			else {
				System.out.println();
				System.out.println("vous avez perdus contre des bactéries, shitttt...");
			}
			System.out.println("le nombre de foyer d'infection est de :"+Town.getNbFoyers());
			System.out.println(" le nombre de remede decouvert est de :"+Disease.getNbRemedy());
			this.displayInfectionInTowns();
		}
		else {
			System.out.println("Dommage...prenez l'air, et revenez vous detendre avec vos potos quand vous voulez");
		}
	}
	
	
}
