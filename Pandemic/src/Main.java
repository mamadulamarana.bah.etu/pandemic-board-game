import java.io.FileNotFoundException;
import java.util.*;

import carte.Card;
import carte.deck.Deck;
import carte.deck.InfectionDeck;
import map.ClassicalMappe;
import map.JSONReader;
import map.Mappe;
import Player.Player;
import Player.PlayerCardUnavailableException;
import Player.StationUnavailableException;
import Player.role.*;
import town.Town;
import virus.Disease;
import virus.DiseaseName;

public class Main {
	private static final Random RAND = new Random();
	
	public static void main(String[] args) {
		try {
			Mappe map = new ClassicalMappe(new JSONReader(args[0])); /** Creation d'une map a partir d'un fichier JSON donnée en paramètre pour le jeu */
			Game game = new Game(map);
				
			Role r1 = new Expert("expert", map);
			Role r2 = new Medecin("medecin", map);
			Role r3 = new GlobeTrotteur("GlobeTrotteur", map);
			Role r4 = new Scientist("scientist", map);
			
			List<Role> roles = new ArrayList<>();
			roles.add(r1); roles.add(r2); roles.add(r3); roles.add(r4);
					
			Player p1 = new Player("Mamad");
			Player p2 = new Player("Matheis");
			Player p3 = new Player("Rayan");
			Player p4 = new Player("Alois");
			
			game.setRoles(roles);
			
			game.addPlayer(p1);
	        game.addPlayer(p2);
	        game.addPlayer(p3);
	        game.addPlayer(p4);
	                        
	        game.play();
		}catch(FileNotFoundException e) {
			System.out.println("le fichier n'a pas été trouvé...Verifier votre saisie");
		}
		
		
		
	}

}
