package carte;
import virus.*;
import town.Town;

public class NormalCard extends Card{
	/**
	protected Town town;
	protected Disease disease;
	*/
	public NormalCard(Town town, Disease disease) {
		super(town, disease);
	}
	
	/**
	 * Use to make the effect of the card
	 * If it's an infection card, it will infect the city linked
	 * If it's a card player, nothing will happen
	 * @return False if we don't keep the card in our hand
	 * @return True if we keep this card in our hand
	 * @throws NotAvailableCubeException 
	 */
	public boolean use() throws NotAvailableCubeException {
		if(this.infection){
			this.town.infection(this.disease, 1);
			return false;
		}
		return true;
	}			
	
}

