package carte.deck;
import java.util.*;

import carte.Card;
import carte.EpidemicCard;
import carte.NormalCard;
import map.*;
import town.Town;
import virus.Disease;
import virus.DiseaseName;

/**
 * The deck of player's and Epidemic card and also the deck of discard's card
 */
public class EpidemicDeck extends Deck{



	/**
	 * This deck is define by a stack, the map of the game
	 * When a deck is created, it call the private methode initdeck()
	 * @param map The map to which deck is linked
	 * @param map
	 */
	public EpidemicDeck(Mappe map) {
		super(map);
		this.deckEpidemic = true;
	}
		
	/**
	 * initDeck is call when a Deck is created. 
	 * It used to initialize the deck by creating all the card by getting the disease and the town of the map and pushing those card in the stack 
	 * It's also calling the private methode setEpidemicCard() and shuffle()
	 */
	public void initDeck() {
		for (int i=0; i<Deck.NB_EpidemicCard; i++) {
			Card card = new EpidemicCard();
			this.stack.push(card);
		}
	}
		
	/**
	 * description of class deckEpidemic
	 */
	public String toString() {
		return "Paquets de cartes epidemic";
	}
	

}





