package carte.deck;
import java.util.*;


import carte.Card;
import carte.NormalCard;
import map.*;
import town.Town;
import virus.Disease;
import virus.NotAvailableCubeException;

/**
 * The deck of player's and Epidemic card and also the deck of discard's card
 */
public class Deck {
	
	protected Stack<Card> stack;
	protected Mappe map;
	protected Town city;
	protected boolean deckInfection;
	protected boolean deckEpidemic;
	protected static final int NB_EpidemicCard = 4;
	
	
	/**
	 * This deck is define by a stack, the map of the game, and the attribute deckInfection at false. 
	 * When a deck is created, it call the private methode initdeck()
	 * @param map The map to which deck is linked
	 * @param map
	 */
	public Deck(Mappe map) {
		this.stack = new Stack<>();
		this.map = map;
		this.deckInfection = false;
		this.deckEpidemic = false;
		this.initDeck();
	}
		
	/**
	 * initDeck is call when a Deck is created. 
	 * It used to initialize the deck by creating all the card by getting the disease and the town of the map and pushing those card in the stack 
	 * It's also calling the private methode setEpidemicCard() and shuffle()
	 */
	public void initDeck() {
		for(Town town : this.map.getTowns()) { //Ajout des cartes player
			Disease disease = this.map.getDiseases()[town.getSector()];
			Card card = new NormalCard(town, disease);
			this.stack.push(card);
		}
		this.stack = this.shuffle(this.stack);
	};
	
	/**
	 * Shuffle is used for shuffle the deck, after initDeck() initialize it
	 */
	protected Stack<Card> shuffle(Stack<Card> stack) {
		List<Card> tmp = new ArrayList<>();
		while ((stack.size()) > 0) {
			tmp.add(stack.pop());
		}
		Collections.shuffle(tmp);
		stack.addAll(tmp);
		return stack;
	}
	
	/**
	 * Préparation finale des piles de cartes joueur
	 */
	public void finalInitPlayerCard() throws EmptyStackException{
		List<Card> tmp1 = new ArrayList<>();
		Stack<Card> tmp2 = new Stack<>();
		int nb_card = this.stack.size()/4;
		int deck_size = this.stack.size();
		while ((this.stack.size()) > 0 || tmp2.size() < (deck_size + Deck.NB_EpidemicCard) ) {
			if (tmp1.size() < nb_card) {
				tmp1.add(this.stack.pop());
			}
			else {
				tmp2.addAll(tmp1);
				tmp2.add(this.map.getDeckEpidemic().pick());
				tmp2 = this.shuffle(tmp2);
				tmp1.removeAll(tmp1);
			}
		}
		this.stack = tmp2;
	}
	
	
	/**
	 * Use to know if the deck is a deck of infection card or not
	 * @return (true) if deck is an infection deck, (false) or not.
	 */
	public boolean isDeckInfection() {
		return this.deckInfection;
	}
	
	/**
	 * Use to know if the deck is a deck of epidemic card or not
	 * @return (true) if deck is an epidemic card deck, (false) or not.
	 */
	public boolean isDeckEpidemic() {
		return this.deckEpidemic;
	}
	
	/**
	 * Use to know if the deck is a deck of player card or not
	 * @return (true) if deck is an player card deck, (false) or not.
	 */
	public boolean isDeckPlayer() {
		return !(this.isDeckEpidemic() || this.isDeckInfection());
	}
	
	/**
	 * Use to return the stack of the deck
	 * @return the stack compose of Players card and 
	 */
	public Stack<Card> getStack() {
		return this.stack;
	}
	
	
	/**
	 * Use to pick a card in the deck
	 * @throws EmptyStackException if there are no card left in the deck
	 */
	public Card pick() throws EmptyStackException {
		if (this.stack.size() > 0) {
			Card card = this.stack.pop();
			return card;
		}
		else {
			throw new EmptyStackException();
		}
	}
		
	/**
	 * Use to get the map
	 * @return the map
	 */
	public Mappe getMap() {
		return this.map;
	}

	/**
	 * description of class deckPlayer
	 */
	public String toString() {
		return "Paquets de cartes Joueurs";
	}
	
}





/**public void initDeck() {
List<Town> towns = this.map.getTowns();
for(int i=0;i<this.nbCarte;i++) {
	city=towns.get(i);
	new NormalCard(city,,this.infection)
	stack.add();
}			

}**/
