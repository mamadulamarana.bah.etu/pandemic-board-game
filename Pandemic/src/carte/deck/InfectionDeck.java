package carte.deck;
import java.util.*;


import carte.Card;
import carte.NormalCard;
import map.Mappe;
import town.Town;
import virus.Disease;

/**
 * The deck of infection's card
 */
public class InfectionDeck extends Deck{
	
	/**
	 * This deck inherit of Deck, he's define by the value of DeckInfection = true
	 * @param map
	 */
	public InfectionDeck(Mappe map) {
		super(map);
		this.deckInfection = true;
	}
	
	/**
	 * initDeck is call when a Deck is created
	 * It used to initialize the deck by creating all the infection card
	 */
	public void initDeck() {
		for(Town town : this.map.getTowns()) {
			Disease disease = this.map.getDiseases()[town.getSector()];
			Card card = new NormalCard(town, disease);
			card.setInfectionCard();
			this.stack.push(card);
		}
		this.stack = this.shuffle(this.stack);
	}
	
	
	/**
	 * description of class deckInfection
	 */
	public String toString() {
		return "Paquets de cartes Infection";
	}
	
}
