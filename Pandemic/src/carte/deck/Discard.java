package carte.deck;

import java.util.*;

import carte.Card;

/**
 * the Discard is where the cart are throw where they're not usefull
 */
public class Discard {
	protected List<Card> discard;
	protected boolean epidemicDiscard;
	protected boolean infectionDiscard;
	
	/**
	 * Discard is define by an ArrayList, and 2 boolean, infection discard and epidemic discard set as fault
	 * it's used when we have to drop player cards
	 */
	public Discard() {
		this.discard = new ArrayList<>();
		this.infectionDiscard = false;
		this.epidemicDiscard = false;
	}
	
	/**
	 * Discard of epidemic Card or infection Card, depending on the value of the parameter
	 * @param if (true) a discard of infectionCard is built, else a discard of epidemicCard is built
	 */
	public Discard(boolean infectionDiscard) {
		this.infectionDiscard = infectionDiscard;
		this.epidemicDiscard = !infectionDiscard;
		this.discard = new ArrayList<>();
	}
	
	/**
	 * discard a used card
	 * @param card
	 */
	public void addCard(Card card) {
		this.discard.add(card);
	}
	
	public List<Card> getDiscard() {
		return this.discard;
	}
	
	/**
	 * check if discard is an infectionDiscard.
	 * @return (true) if discard is an infectionDiscard, (false) or not.
	 */
	public boolean isEpidemicDiscard() {
		return this.epidemicDiscard;
	}
	
	/**
	 * check if discard is an epidemicDiscard.
	 * @return (true) if discard is an epidemicDiscard, (false) or not.
	 */
	public boolean isinfectionDiscard() {
		return this.infectionDiscard;
	}
	
	/**
	 * description for disacrd
	 */
	public String toString() {
		if (this.epidemicDiscard) {
			return "Epidemic card discard";
		}
		else if (this.infectionDiscard) {
			return "Infection Card discard";
		}
		else {
			return "Player Card discard";
		}
	}

}
