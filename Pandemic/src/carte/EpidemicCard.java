package carte;

/**
 * Use for epidemic card, herit from Card
 */
public class EpidemicCard extends Card{
	
	private boolean epidemic;
	/**
	 * Build an epidemic card with epidemic (true) and infection (false) boolean
	 */
	public EpidemicCard() {
		super();
		this.epidemic = true;
		this.infection = false;
	}
	
	/**
	 * Use to make the effect of the card
	 * Add 1 to the global infection  because it's a pendemic card
	 * @return false because we don't keep it in the hand
	 */
	public boolean use() {
		//this.deckInfection.addGlobalInfection(1);
		return false;
	}

	
	/**
	 * Use to know if this a epidemic card
	 * @return epidemic
	 */
	public boolean isEpidemic() {
		return this.epidemic;
	}
	
	/**
	 * Use to know what card is it
	 * @return A text that describ the type of the card
	 */
	public String toString() {
		return "carte epidemic";
	}
}

