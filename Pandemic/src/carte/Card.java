package carte;

import town.Town;
import virus.Disease;
import virus.DiseaseName;
import virus.NotAvailableCubeException;
/**
 * Card is an abstract class
 */
public abstract class Card {
	
	protected boolean infection;
	protected boolean epidemic;
	protected Town town;
	protected Disease disease;
	
	/**
	 * builds a card 
	 */
	public Card() {
		this.infection = false;
		this.epidemic = false;
	}
	
	/**
	 * builds a card  (playerCard, epidemicCard or infectionCard)
	 * @param town
	 * @param disease
	 */
	public Card(Town town, Disease disease) {
		this.town = town;
		this.disease = disease;
		this.infection = false;
		this.epidemic = false;
	};

	/**
	 * Asbtract function, use to use the card, according to its properties
	 * @return false if we don't keep that card in your hand 
	 * @return true if you will keep that card in your hand
	 * @throws NotAvailableCubeException 
	 */
	public abstract boolean use() throws NotAvailableCubeException;
	
	/**
	 * Use to know if it's an Epidemic card or not 
	 * @return
	 */
	public boolean isEpidemic() {
		return this.epidemic;
	}
	
	/**
	 * Use to change infection attributes to true 
	 */
	public void setInfectionCard() {
		this.infection = true;
	}
	
	/**
	 * Use to know if this is an infection card
	 * @return infection
	 */
	public boolean isInfectionCard() {
		return this.infection;
	}
	
	/**
	 * Use to know if this is an Player card
	 * @return infection
	 */
	public boolean isPlayerCard() {
		return !(this.isInfectionCard() || this.isEpidemic());
	}
	
	/**
	 * Use to get the town of the card
	 * @return the town of the card
	 */
	public Town getTown() {
		return this.town;
	}
	
	/**
	 * Use to get the name of the disease of the card
	 * @return the disease name
	 */
	public DiseaseName getDiseaseName() {
		return this.disease.getName();
	}
	
	/**
	 * Use to get the disase of the card
	 * @return the disease of the card
	 */
	public Disease getDisease() {
		return this.disease;
	}


	/**
	 * 
	 */
	public String toString() {
		return "Carte " + this.disease.getName() + " de " + this.getTown();
	}
	
	/**
	 * equals
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Card)) {
			return false;
		}
		else {
			Card other = (Card)o;
			return this.getTown().equals(other.getTown()) && this.getDisease().equals(other.getDisease());
		}
	}
}

