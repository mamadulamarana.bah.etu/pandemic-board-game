/** NB : Ebauche (peut ne pas être nécessaire) )**/ 

package town;

import virus.*;

import java.util.*;

import Player.Player;

public class Town {
	protected String name;
	protected int sector;
	protected Map<Disease, Integer> diseases;
	private boolean isOutbreak;
	private boolean station;
	protected List<Town> neighbors;
	protected List<Player> players;
	protected static final int NB_CUBE_MAX = 3;
	protected static final int NB_STATION = 6;
	protected static int nb_Station;
	protected static int nb_Foyers;
	
	/**
	 * builds a town with given name and sector
	 * @param name of the town to build
	 * @param sector the sector of the town to build
	 */
	public Town(String name, int sector) {
		this.name = name;
		this.sector =  sector;
		this.isOutbreak = false;
		this.station = false;
		Town.nb_Foyers = 0;
		Town.nb_Station = 0;
		this.diseases = new HashMap<>();
		this.neighbors = new ArrayList<>();
		this.players = new ArrayList<>();
	}

	/**
	 * get the name of the town
	 * @return town name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * set a player on this town
	 */
	public void setPlayer(Player p) {
		this.players.add(p);
	}
	
	/**
	 * 
	 * @param p
	 */
	public void removePlayer(Player p) { /** modif uml **/
		this.players.remove(p);
	}
	/**
	 * get the number of cubes infection
	 * @return number of cube Infection
	 */
	public int getNbDisease() {
		return this.diseases.keySet().size();
		
	}
	
	/**
	 * 
	 * @param map the game map with all the cities
	 */
	public void setNeighbors(List<Town> neighbors) {
		this.neighbors = neighbors;
	}
	
	/**
	 * get this town neighbors
	 * @return this town neighbors
	 */
	public List<Town> getNeighbors() {
		return this.neighbors;
	}
	
	/**
	 * get sector of this town
	 * @return sector of this city
	 */
	public int getSector() {
		return this.sector;
	}
	
	/**
	 * to know this city is an outbreak or not
	 * @return (true) if this city is an outbreaks, false or not
	 */
	public boolean isOutbreak() {
		return this.isOutbreak;
	}
	
	/**
	 * to know if in this town there are a search station
	 * @return (true) if there are a search station, false or not
	 */
	public boolean isStation() {
		return this.station;
	}
	
		
	/**
	 * builds a search station in this town
	 * (true)set a search station,(false) remove search station
	 */
	public boolean setStation() {
		if (Town.nb_Station < Town.NB_STATION) {
			this.station = true;
			Town.nb_Station++;
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * get the number of built station in the game
	 * @return number of built station
	 */
	static public int getNbStation() {
		return Town.nb_Station;
	}
	
	/**
	 * remove a station in this town
	 */
	public void removeStation() { /** modif uml **/
		this.station = false;
		Town.nb_Station--;
	}
	
	/**
	 * get all players who are on this city
	 * @return list of the players who are on this city
	 */
	public List<Player> getPlayers() {
		return this.players;
	}
	
	/**
	 * make this city an outbreak of infection
	 * @param outbreak (true)set a outbreak,(false) remove search station
	 */
	public void setOutbreak(boolean outbreak) {
		this.isOutbreak= outbreak;
	}
		
	/**
	 * get all the disases that infect this town
	 * @return disases that infect this town
	 */
	public Map<Disease, Integer> getDiseases() {
		return this.diseases;
	}
	
	/**
	 * get the number of foyers in the game (method static)
	 * @return the number of cities that have become outbreak
	 */
	public static int getNbFoyers() {
		return Town.nb_Foyers;
	}
	
	/**
	 * spread the infection to neighboring cities because
	 * this city has become a focus of infection
	 * @param disease disease disease with which we will infect the neighbors
	 * @throws NotAvailableCubeException there are no more cube available
	 * 
	 */
	private void propageInfection(Disease disease) throws NotAvailableCubeException {
		System.out.println("et elle va propager la maladie "+disease);
		for (Town neighbor: this.getNeighbors()) {
			if ( !neighbor.isOutbreak() ) {
				System.out.print("voisin de la ville "+ this + ", ");
				neighbor.infection(disease, 1);
			}
			else {
				System.out.println(neighbor + " est deja un foyer d'infection, pas d'infection durant cette phase");
			}
		}
	}
	
	/**
	 * infection of this city and increase the number of infection cubes
	 * if it is not yet an infection focus
	 * @param disease Cube
	 * @throws NotAvailableCubeException (plus de cubes disponible), lancé par la classe disease
	 */
	public void infection(Disease disease, int nbCubes) throws NotAvailableCubeException{
		if ( !disease.isEradicate() && disease.CanSetCube() ) {
			if ( !this.diseases.containsKey(disease) ) {
				this.diseases.put(disease, nbCubes);
        		System.out.println("la "+this+" a ete infecte par la maladie "+disease);
			}
			else if( (this.diseases.get(disease) == Town.NB_CUBE_MAX) && !this.isOutbreak() ) { 
			 	this.setOutbreak(true);
			 	System.out.println("tentative d'infection de la "+ this+ " par la maladie "+disease);
			 	System.out.print(this +" est devenu un foyer d'infection ");
			 	Town.nb_Foyers++;
			 	this.propageInfection(disease);
			}
			else if( this.isOutbreak() ){
				System.out.println(this + " est deja un foyer d'infection donc pas de contamination pendant cette phase d'infection");
			}else {
				this.diseases.put(disease, this.diseases.get(disease)+ nbCubes);
	        	System.out.println("la "+this+" a ete infecte par la maladie "+disease);
			 }
		}else {
			System.out.println("cette maladie "+disease+" a ete eradique, pas d'infection possible");
		}
	}
	

	/**
	 * method equals
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Town)) {
			return false;
		}
		else {
			Town other = (Town)o;
			return this.getName().equals(other.getName()) && this.getSector() == other.getSector();
		}
	}
	
	/**
	 * Description Of the town
	 */
	public String toString() {
		return ""+this.getName();
	}
}


