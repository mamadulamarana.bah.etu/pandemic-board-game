package town;

public class OutBreakException extends Exception {

	public OutBreakException() {
	}

	public OutBreakException(String message) {
		super(message);
	}
}
