import java.io.FileNotFoundException;

import map.*;

import town.*;
import util.io.Input;
import virus.Disease;
import virus.*;

/**
 * 
 * @author g1
 * Main pour le livrable 1, implementation de Map et Town
 *
 */
public class MainLv1 {

	public static void main(String[] args) {
		try {
			Mappe map = new ClassicalMappe(new JSONReader(args[0])); /** Creation d'une map a partir d'un fichier JSON donnée en paramètre pour le jeu */
			
			System.out.println("Voici Les différentes Villes de la Map du jeu :");
			System.out.println(map.getTowns());
			System.out.println();
			System.out.println("voulez-vous obtenir la liste des voisins d'une ville du jeu ? (oui) ou (non)");
			String readString = Input.readString().toLowerCase();
			
			while (readString.equals("oui")) {
				System.out.println("Veuillez saisir son nom parmis celle presente dans la liste ci-dessus svp, ex: ville-1, ville-2, ville-13,....");
				String townName = Input.readString();
				
				for (Town town : map.getTowns()) {
					if (town.getName().equals(townName)) {
						System.out.println(town.getNeighbors());
					}
				}
				
				System.out.println("voulez-vous obtenir la liste des voisins d'une autre ville du jeu ? (oui) ou (non)");
				readString = Input.readString().toLowerCase();
			}
			System.out.println("Vous pourriez y jouer plus Tard a Pandemic. Merci et a Bientot");
		}catch(FileNotFoundException e) {
			System.out.println("le fichier n'a pas été trouvé...Verifier votre saisie");
		}

		}

}
