package Player.role;

public class StationAvailableException extends Exception {

	public StationAvailableException() {
	}

	public StationAvailableException(String message) {
		super(message);
	}


}
