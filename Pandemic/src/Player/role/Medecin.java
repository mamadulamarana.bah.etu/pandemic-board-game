package Player.role;

import java.util.ArrayList;
import java.util.List;

import Player.Player;
import map.Mappe;
import town.Town;
import util.listchooser.InteractiveListChooser;
import util.listchooser.ListChooser;
import virus.Disease;

/**
 * The medecin can remove all the cubes of a disease from a city at once, even without a cure for this city
 * The medecin can also remove all cubes as soon as he passes throught a city where there are cubes of a cured disease, without counting this as an action
 */
public class Medecin extends Role {
	
	/**
	 * Use to create a Medecin
	 * @param name The name of the Medecin
	 * @param map The map where the role is going to be
	 */
	public Medecin(String name, Mappe map) {
		super(name, map);
	}

	/**
	 * Use to treat, the function  has been modificated for the medecin role.
	 * @param p the Player
	 */
	public void treat(Player p) {
		try {
			List<Disease> townDiseases = new ArrayList<>(p.getTown().getDiseases().keySet());
			ListChooser<Disease> lc = new InteractiveListChooser<>();
			Disease disease = lc.choose("quelle maladie voulez vous traitez ?", townDiseases);
			int nb_removedCubes = 0;
			if (disease.isCured() ) {
				nb_removedCubes = this.treatCuredDisease(p.getTown(), disease);
			}
			else {
				nb_removedCubes = p.getTown().getDiseases().get(disease);
				p.getTown().getDiseases().remove(disease);
				disease.resetCube(nb_removedCubes);
				p.increaseNbAction();
				System.out.println(nb_removedCubes + " cubes de la maladie "+disease +" a ete retire de la "+p.getTown());
			}
		} catch(NullPointerException e) {
			System.out.println("cette ville n'est infecte par aucune maladie, choisissez une autre action");
		}
	}
	
	
	
}
