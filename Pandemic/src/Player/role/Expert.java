package Player.role;

import java.util.Iterator;


import Player.Player;
import Player.PlayerCardUnavailableException;
import carte.Card;
import map.Mappe;
import town.Town;
import util.listchooser.InteractiveListChooser;

/**
 * The expert, he can build a station without any playerCard
 */
public class Expert extends Role{

	/**
	* Use to build an the "Expert" role
	* @param name The name of the Expert
	* @param map The map where the role is going to be
	*/
	public Expert(String name, Mappe map) {
		super(name, map);
	}
	
	/**
	 * Use to build a station on the city that the expert is. But the expert don't need any player card to build it
	 * @param p The player
	 * @throws StationAvailableException When a station is already here
	 */
	public void build(Player p) throws StationAvailableException{
		if (!p.getTown().isStation()) {
			if ( !p.getTown().setStation() ){
				InteractiveListChooser<Town> ilc = new InteractiveListChooser<>();
				Town townStation = ilc.choose("de quelle ville voulez vous deplacer une station ?", this.getTownsWithStation());
				townStation.removeStation();
				p.getTown().setStation();
			}
			p.increaseNbAction();
			System.out.println(p + " a construit une station dans la "+ p.getTown());
		}
		else {
			throw new StationAvailableException("une station existe deja dans cette ville. choisissez une autre action");
		}
	}
	

}
