	package Player.role;

import carte.*;

import map.Mappe;

import java.util.*;

import Player.Player;
import Player.PlayerCardUnavailableException;
import Player.StationUnavailableException;
import virus.* ;

import town.Town;
import util.listchooser.InteractiveListChooser;
import util.listchooser.ListChooser;

/**
 * Basic role, Use to define all the fondamental action every role could do (without the specific one for each role)
 */
public class Role {
	protected String name;
	protected List<Player> players;
	protected Mappe map;
	protected static final int NB_CUBES = 24;
	protected int nb_required_card_for_cure;

	/**
	* Create a role  
	* @param name The name of the Player
	* @param map Map where the Player will be 
	*/
	public Role(String name, Mappe map) {
		this.name = name;
		this.map = map;
		this.nb_required_card_for_cure = 5;
		this.players = new ArrayList<>();
	}
		
	/**
	 * Use to return the role of the player
	 * @return the role of the player
	 */
	public String getPlayerRole() {
		return this.name;
	}
	
	/**
	 * Use to know how many card this role need to cure
	 * @return the number of card that this role need to cure 
	 */
	public int getNbRequiredCard() {
		return this.nb_required_card_for_cure;
	}
			
	/**
	 * Use to add a attribute a role to a player, in fact it's use to know the number of players 
	 */
	public void setPlayer(Player p) {
		this.players.add(p);
	}
	
	
	// ACTION 
	/**
	 * Use to get the map where the player is
	 * @param map the map
	 * @return the map
	 */
	public Mappe getMap(Mappe map) {
		return this.map;
	}
		
	/**
	 * Use to have the get the towns with their station (if they add one)
	 * @return the tonws and their station
	 */
	protected List<Town> getTownsWithStation() {
		List<Town> towns = new ArrayList<>();
		for (Town town : this.map.getTowns()) {
			if (town.isStation()) {
				towns.add(town);
			}
		}
		return towns;
	}
		
	/**
	 * Use to make a player mooving in a city that is a neighbors of the city that he's already on
	 * @param p
	 */
	public void move(Player p) {
		Town playerTown = p.getTown();
		List<Town> townNeighBors = playerTown.getNeighbors();
		ListChooser<Town> ilc = new InteractiveListChooser<>();
		Town destinationTown = ilc.choose("Dans quel ville voulez vous vous deplacer ?", townNeighBors);
		p.setTown(destinationTown);
		playerTown.removePlayer(p);
		destinationTown.setPlayer(p);
		p.increaseNbAction();
		System.out.println(p + " qui etait sur la "+ playerTown + " s'est deplace a la "+ p.getTown());
	}
	
	/**
	 * Use to build a station, in order to build it you must have in your hand a player card whose city is the one on which you are located.
	 * However, the number of search stations is limited. If there are no more available, you have to move one of them from another city.
	 * @throws PlayerCardUnavailableException when the player don't have the required player's card
	 * @throws StationAvailableException when a station is already build in this city
	 */
	public void build(Player p) throws PlayerCardUnavailableException, StationAvailableException {
		if (!p.getTown().isStation()) {
			Iterator <Card> it = p.getMain().iterator();
			Card card = it.next();
			while (it.hasNext() && (card.getTown() != p.getTown()) ) {
				card = it.next();
			}
			
			if (card.getTown() == p.getTown()) {
				if( !p.getTown().setStation() ) {
					InteractiveListChooser<Town> ilc = new InteractiveListChooser<>();
					Town townStation = ilc.choose("Plus de station disponible, de quelle ville voulez vous deplacer une station ?", this.getTownsWithStation());
					townStation.removeStation();
					card.getTown().setStation();
				}
				p.increaseNbAction();
				this.map.getNormalDiscard().addCard(card);
				p.getMain().remove(card);
				System.out.println(p + " a construit une station dans la "+ p.getTown());
			}
			else {
				throw new PlayerCardUnavailableException("vous n'avez pas les cartes joueurs requis, choisissez une autre action");
			}
		}
		else {
			throw new StationAvailableException("une station existe deja dans cette ville. choisissez une autre action");
		}
	}
	
	/** 
	 * Use to discard the used Card in order to Cure
	 * @param p The player
	 * @param disease The disease
	 * @param nbCard The number of card
	 */
	protected void discardUsedCardForCure(Player p, Disease disease, int nbCard) {
		Iterator<Card> it = p.getMain().iterator();
		it = p.getMain().iterator();
		while (it.hasNext() && nbCard != 0) {
			Card card = it.next();
			if (card.getDisease().equals(disease)) {
				this.map.getNormalDiscard().addCard(card);
				it.remove();
				nbCard--;
			}
		}
	}
	
	/**
	 * Check the player's hand to see if he has the required number of cards
	 * @param p The player
	 * @param disease The disease
	 * @return true If there is enought card , false if there is not 
	 */
	protected boolean checkPlayerMainForCure(Player p, Disease disease, int nbCard) {
		Iterator <Card> it = p.getMain().iterator();
		int nbPlayerCard = 0;
		while (it.hasNext() && (nbPlayerCard < nbCard) ) {
			Card card = it.next();
			if ( card.getDisease().equals(disease) ) {
				nbPlayerCard++;
			}
		}
		if (nbPlayerCard == nbCard) {
			return true;
		}
		return false;
	}

	
	/**
	 * Action use to Cure a disease from a city
	 * @param p The player
	 * @throws PlayerCardUnavailableException If there isn't the required number of cards player to cure this city
	 * @throws StationUnavailableException If there isn't station in this city
	 */
	public void Cure(Player p) throws PlayerCardUnavailableException, StationUnavailableException {
		if (p.getTown().isStation()) {
			InteractiveListChooser<Disease> ilc = new InteractiveListChooser<>();
			Disease disease = ilc.choose("quelle maladie voulez-vous guerir ?", p.getDiseasesOnHisHand());
			if ( this.checkPlayerMainForCure(p, disease, this.nb_required_card_for_cure) ) {
				this.discardUsedCardForCure(p, disease, this.nb_required_card_for_cure);
				disease.setCured();
				p.increaseNbAction();
				System.out.println("la maladie "+disease+" a ete guerie, un remede a ete decouvert");
			}
			else {
				throw new PlayerCardUnavailableException("vous n'avez pas les cartes joueurs requis, choisissez une autre action"); 
			}
		}
		else {
			throw new StationUnavailableException("vous ne pouvez pas guérir de maladie car pas de station disponible dans cette ville, choisissez une autre action");
		}
	}
	

	/**
	 * Use to treat a cured disease
	 * @param town The city where the disease has been cured
	 * @param disease The disease to treat
	 * @return The number of removed cubes
	 */
	protected int treatCuredDisease(Town town, Disease disease) {
		int nb_removedCubes = town.getDiseases().get(disease);
		town.getDiseases().remove(disease);
		disease.resetCube(nb_removedCubes);
		return nb_removedCubes;
	}
	
	/**
	 * Use to treat a non cured disease
	 * @param town The city where the disease is
	 * @param disease The disease to treat
	 * @return The number of removed cubes
	 */
	private int treatNotCuredDisease(Town town, Disease disease) {
		if ( town.getDiseases().get(disease) > 1 ) {
			town.getDiseases().put(disease, town.getDiseases().get(disease) - 1);
		}
		else {
			town.getDiseases().remove(disease);
		}
		int nb_removedCubes = 1;
		disease.resetCube(nb_removedCubes);
		return nb_removedCubes;
	}
	
	/**
	 * Use to treat a disease 
	 * @param p the player who is doing this action
	 */
	public void treat(Player p) {
		try {
			List<Disease> townDiseases = new ArrayList<>(p.getTown().getDiseases().keySet());
			ListChooser<Disease> lc = new InteractiveListChooser<>();
			Disease disease = lc.choose("quelle maladie voulez vous traitez ?", townDiseases);
			int nb_removedCubes = 0;
			if (disease.isCured()) {
				nb_removedCubes = this.treatCuredDisease(p.getTown(), disease);
			}
			else {
				nb_removedCubes = this.treatNotCuredDisease(p.getTown(), disease);
			}
			p.increaseNbAction();
			System.out.println(nb_removedCubes + " cubes de la maladie "+disease +" a ete retire de la "+p.getTown());
		}catch(NullPointerException e) {
			System.out.println("cette ville n'est infecte par aucune maladie, choisissez une autre action");
		}
	}
	
	/**
	 * Use to get the name
	 * @return the name
	 */
	public String toString() {
		return "l'"+this.name;
	}
	
	/**
	 * Use to know if two role are equals
	 * @return true if there are equals, false if they're not
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Role)) {
			return false;
		}
		else {
			Role other = (Role)o;
			return this.name.equals(other.name);
		}
	}		
	
	
}
