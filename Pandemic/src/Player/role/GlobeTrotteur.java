package Player.role;

import java.util.List;

import Player.Player;
import map.Mappe;
import town.Town;
import util.listchooser.InteractiveListChooser;
import util.listchooser.ListChooser;

/**
 * The GlobeTrotteur can  move in any city
 */
public class GlobeTrotteur extends Role {

	/**
	 * Create a globeTrotteur
	 * @param name The name of the globeTotteur
	 * @param map The map where the role is going to be
	 */
	public GlobeTrotteur(String name, Mappe map) {
		super(name, map);
	}

	/**
	 * Use to move in any city
	 * @param p the player
	 */
	public void move(Player p) {
		Town playerTown = p.getTown();
		ListChooser<Town> lc = new InteractiveListChooser<>();
		Town destinationTown = lc.choose("Dans quel ville voulez vous vous deplacer ?", this.map.getTowns());
		p.setTown(destinationTown);
		playerTown.removePlayer(p);
		destinationTown.setPlayer(p);
		p.increaseNbAction();
		System.out.println(p + " qui etait sur la "+ playerTown + " s'est deplace a la "+ p.getTown());
	}

}
