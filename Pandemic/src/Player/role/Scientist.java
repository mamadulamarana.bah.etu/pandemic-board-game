package Player.role;

import Player.Player;
import Player.PlayerCardUnavailableException;
import Player.StationUnavailableException;
import map.Mappe;
import util.listchooser.InteractiveListChooser;
import virus.Disease;

/**
 * The scientist only need 4 player card of the same disease in order to cure it
 */
public class Scientist extends Role {

	
	protected int nb_required_card_for_cure;

	/**
	 * Use to create a scientist
	 */
	public Scientist(String name, Mappe map) {
		super(name, map);
		this.nb_required_card_for_cure = 4;
	}
	
	/**
	 * Use to cure a disease, but here the number of required card for cure is only 4
	 * @param p
	 * @throws PlayerCardUnavailableException
	 * @throws StationUnavailableException
	 */
	public void Cure(Player p) throws PlayerCardUnavailableException, StationUnavailableException {
		super.Cure(p);
	}
	
	
	
}




//if (p.getTown().isStation()) {
//	InteractiveListChooser<Disease> ilc = new InteractiveListChooser<>();
//	Disease disease = ilc.choose("quelle maladie voulez-vous guérir ?", p.getDiseasesOnHisHand());
//
//	if ( this.checkPlayerMain(p, disease) == this.nb_required_card_for_cure) {
//		this.discardUsedCard(p, disease);
//		disease.setCured();
//		p.increaseNbAction();
//		System.out.println("la maladie "+disease+" a ete guerie, un remede a ete decouvert");
//	}
//	else {
//		throw new PlayerCardUnavailableException("vous n'avez pas les cartes joueurs requis, choisissez une autre action"); 
//	}
//}
//else {
//	throw new StationUnavailableException("vous ne pouvez pas guérir de maladie car pas de station disponible dans cette ville, choisissez une autre action");
//}
