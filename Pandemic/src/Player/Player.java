package Player;
import java.util.*;


import Player.role.*;
import carte.Card;
import carte.deck.Deck;
import carte.deck.Discard;
import map.Mappe;
import town.Town;
import util.listchooser.InteractiveListChooser;
import util.listchooser.ListChooser;
import virus.Disease;
import virus.DiseaseName;
import virus.NotAvailableCubeException;



public class Player {
	protected String name;
	protected Role role;
	protected List<Card> main;
	protected List<String> action;
	protected int nbCard;
	protected int nb_action;
	protected static final int NB_ACTION = 4;
	protected Town town;
	protected final static int MAIN_Limit = 7;
	//protected static final Random RAND = new Random();
	
	/**
	 * Use to create a player, each player has a name, a role, a hand, 5 specific action, a town, a number of card and a number of action
	 * @param name The player's name
 	 */
	public Player(String name) {
		this.name=name ;
		this.role = null;
		this.main = new ArrayList<>();
		this.action = new ArrayList<>();
		this.action.add("move"); this.action.add("build"); this.action.add("treat"); this.action.add("cure"); this.action.add("ne rien faire");
		this.town = null;
		this.nbCard = 0;
		this.nb_action = 0;
	}	
		
	/**
	 * Use to get the name of the player
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Use to get the town of the player
	 * @return the town
	 */
	public Town getTown(){
		return this.town;
	}
	
	/**
	 * Use to get the role of the player
	 * @return the role
	 */
	public Role getRole() {
		return this.role;
	}
	
	/** 
	 * Use to set a role to a player
	 * @param roles All the possible role
	 */
	public void setRole(List<Role> roles) {
		ListChooser<Role> ilc = new InteractiveListChooser<>();
		this.role = ilc.choose(this + " veuillez choisir un role s'il vous plait", roles);
		this.role.setPlayer(this);
	}
	
	/**
	 * Ue to attribute a city to this player
	 * @param town the city
	 */
	public void setTown(Town town ) {
		this.town = town ;
		town.setPlayer(this);
	}
	
	/**
	 * Use to get the hand of the player
	 * @return the hand of the player
	 */
	public List<Card> getMain() {
		return this.main;
	}
	
	/**
	 * Use to increase the number of his action's number by 1
	 */
	public void increaseNbAction() { /** modif uml **/
		this.nb_action++;
	}
	
	/**
	 * Use to get the number of this player's action that he made during the game
	 * @return the number of action
	 */
	public int getNbAction() {
		return this.nb_action;
	}
	
	/**
	 * Use to reset the number this player's action
	 */
	public void resetNbAction() {
		this.nb_action = 0;
	}
	
	/**
	 * Use to take a card from a deck
	 * @param decksPlayer The deck where the player have to pick a card
	 * @throws EmptyStackException WHen the deck is empty
	 */
	public Card takeCard(Deck deckPlayer) throws EmptyStackException{
		Card card = deckPlayer.pick();
		return card;
	}
	
	/**
	 * Use to add a card to his hand, if he don't have enought space for it in his hand , he need to throw one card to the discard
	 * @param card The card to add in his hand
	 * @param discard The discard where he need to throw his card
	 */
	public void addCard(Card card, Discard discard){
		if (this.nbCard < MAIN_Limit) {
			this.main.add(card);
			this.nbCard++;
		}
		else {
			ListChooser<Card> ilc = new InteractiveListChooser<>();
			Card removedCard = ilc.choose("quelle carte voulez vous defausser ?", this.main);
			this.main.remove(removedCard);
			this.main.add(card);
			discard.addCard(removedCard);
			System.out.println("la carte "+removedCard+" a ete defaussee");
			System.out.println("la carte "+card+" a ete ajoute a votre main");
			
		}
	}
	
	/**
	 * Use to remove a card of his hand
	 * @param indice The position of the card that he will throw
	 * @return the element previously at the specified position
	 * @throws IndexOutOfBoundsException if the index is out of range (index < 0 || index >= size())
	 * @throws UnsupportedOperationException if  the remove operation is not supported by this list
	 */
	public Card delCard(int indice) throws IndexOutOfBoundsException,UnsupportedOperationException {
		return this.main.remove(indice);
	}

	/**
	 * Use to get a specific card from his hand
	 * @param indice the position of the card in his hand
	 * @return the card
	 */
	public Card getCard(int indice) {
		return this.main.get(indice);
	}
		
	/**
	 * Return a list of the disease on the card that he have in his hand
	 * @return a list of the disease on the card that he have in his hand
	 */
	public List<Disease> getDiseasesOnHisHand() { /** modif uml **/
		List<Disease> diseases = new ArrayList<>();
		for (Card card : this.main) {
			if ( !(diseases.contains(card.getDisease())) ) {
				diseases.add(card.getDisease());
			}
		}
		return diseases;
	}
	
	/**
	 * Use for make a player play one or more action
	 * @throws StationUnavailableException  If there isn't station in this city
	 * @throws PlayerCardUnavailableException  If there isn't the required number of cards player to cure this city
	 * @throws StationAvailableException If there is already a station in this city
	 */
	public  void play() throws PlayerCardUnavailableException, StationUnavailableException, StationAvailableException {
		ListChooser<String> ilc = new InteractiveListChooser<>();
		String choice = "";
		choice = ilc.choose("quelle action voulez vous effectuer", this.action);
		if (choice.equals("move")) {
			this.getRole().move(this);
		}
		else if (choice.equals("cure")) {
			this.getRole().Cure(this);
		}
		else if (choice.equals("treat")) {
			this.getRole().treat(this);
		}
		else if (choice.equals("build")) {
			this.getRole().build(this);
		}
		else {
			System.out.println("vous avez choisi de ne rien faire");
			this.increaseNbAction();
		}
	}

	/**
	 * Use to have the name of the player
	 * @return the name of the player
	 */
	public String toString() {
		return "" + this.name;
	}
	
	/**
	 * Use to know if too players are the same
	 * @return true if they're the same, false if not
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Player)) {
			return false;
		}
		else {
			Player other = (Player)o;
			return this.getName().equals(other.getName());
		}
	}

}
	
//	public void play(int indice ) {
//		if (indice==1) {
//			this.role.move(); 
//		}
//		else if ( indice == 2) {
//			this.role.build();
//		}
//		else if (indice == 3) {
//			this.role.find();
//		}
//		else if (indice ==4) {
//			this.role.treat();
//		}

