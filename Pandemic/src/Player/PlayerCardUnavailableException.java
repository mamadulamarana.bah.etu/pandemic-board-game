package Player;

public class PlayerCardUnavailableException extends Exception {
	
	public PlayerCardUnavailableException() {
		super();
	}
	
	public PlayerCardUnavailableException(String message) {
		super(message);
	}

}
