package Player;

public class StationUnavailableException extends Exception {

	public StationUnavailableException() {
	}

	public StationUnavailableException(String message) {
		super(message);
	}

}
