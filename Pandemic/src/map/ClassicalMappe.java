package map;

import java.util.Collections;

import town.Town;
import virus.DiseaseName;

/**
 * 
 * @author mamad Rayan Matheis Alois
 * class of ClassicalMappe
 */
public class ClassicalMappe extends Mappe {
	
	/**
	 * builds a Classical Mappe for the game
	 * @param jsonFile JSON data containing the different cities and their neighbors
	 */
	public ClassicalMappe(JSONReader jsonFile) {
		super(jsonFile);
	}
	
	/**
	 * (abstract method)
	 * initialization of the map with inclusion of the different cities
	 */
	public void initMap() {
		/** init towns form reader (file Json)*/
		for(String townName: this.reader.getCities().keySet()) {
			this.towns.add(new Town(townName, this.reader.getCities().getInt(townName)) );
		}
		Collections.shuffle(this.towns);
		
		/** init towns NeighBors from reader (file Json)*/
		for (Town town : this.getTowns()) {
			town.setNeighbors(this.getTownNeighbor(town));
		}
	}
	
	
}











//public void initSectors() {
//int i=0;
//for (String town : this.towns.getCities().keySet()) {
//	//Ajouter ici chaque ville au tableau ALlTown
//	
//	int sector = this.towns.getCities().getInt(town);
//	if (! (this.sectors.containsKey(town)) ) {
//		this.sectors.put(sector, new ArrayList<>());
//	}
//	this.sectors.get(sector).add(town);
//}

