package map;

import java.io.FileNotFoundException;

import java.io.FileReader;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * 
 * @author Mamadu Rayan Alois Matheis
 * class JSONReader
 * read and extract data from a file given as argument
 */
public class JSONReader {
	private String filename;
	private JSONObject towns;
	
	/**
	 * builds a file reader
	 * @param the given filename
	 */
	public JSONReader(String filename) throws FileNotFoundException {
		this.filename = filename;
		this.extractData(); /** get the towns in JSON File */
	}
	
	/**
	 * get the towns in JSON File
	 * read a JSONFile and extract given towns
	 */
	private void extractData() throws FileNotFoundException {
		FileReader reader = new FileReader(filename);
		this.towns = new JSONObject(new JSONTokener(reader));
	}
	
	/**
	 * get all data in given JSONFile
	 * @return all data in the JSONFile
	 */
	public JSONObject getTowns() {
		return this.towns;
	}
	
	/**
	 * get all the cities in the JSON file
	 * @return all the cities in the JSON file
	 */
	public JSONObject getCities() {
		return this.towns.getJSONObject("cities");
	}

	/**
	 * get all the neighbors in the JSON file
	 * @return all the neighbors in the JSON file
	 */
	public JSONObject getNeighbors() {
		return this.towns.getJSONObject("neighbors");
	}
	
	/**
	 * Use to have the total number of town
	 * @return The number of the town in the game
	 */
	public int getNumberTown() {
		return this.getCities().keySet().size();
	}	
	
		
		
}

