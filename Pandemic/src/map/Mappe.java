package map;

import java.util.*;

import Player.role.Role;
import carte.Card;
import carte.NormalCard;
import carte.deck.*;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
/**
 * 
 * @author	Mamadu Rayan Matheis Alois
 * class Mappe
 */
public abstract class Mappe {
	protected int nb_Foyer;
	protected int nb_remedy;
	protected JSONReader reader;
	protected List<Town> towns;
	protected Disease [] diseases;
	protected int stations;
	protected Deck deckPlayer;
	protected Deck deckInfection;
	protected Deck deckEpidemic;
	protected Discard normalDiscard;
	protected Discard infectionDiscard;
	protected Discard epidemicDiscard;
	protected List<Role> roles;
	
	
	/**
	 * builds the Mappe for the Pandemic Game with all towns
	 * @param jsonFile JSON data containing the different cities and their neighbors
	 */
	public Mappe(JSONReader jsonFile) {
		this.reader = jsonFile;
		this.towns = new ArrayList<>();
		this.diseases = new Disease[DiseaseName.values().length];
		this.nb_remedy = 0;
		this.initMap(); /** see method initMap */
		this.initDiseases(); /** see method initDisease */
		this.initDecks();	/** see method initDecks */
		this.initDiscards(); /** see method initDiscards */
	}
	
	/**
	 * (abstract method)
	 * initialization of the map with inclusion of the different cities 
	 */
	public abstract void initMap();
	
	/**
	 * Use to init the disease in the map
	 */
	public void initDiseases() {
		int nb_disease = DiseaseName.values().length;
		for (int i = 0; i < nb_disease; i++) {
			this.diseases[i] = new Disease(i);
		}
	}
	
	/**
	 * Use to init the deck in the map
	 */
	public void initDecks() {
		this.deckPlayer = new Deck(this);
		this.deckInfection = new InfectionDeck(this);
		this.deckEpidemic = new EpidemicDeck(this);
	}
	
	/**
	 * use to init differents discard
	 */
	public void initDiscards() {
		this.normalDiscard = new Discard();
		this.infectionDiscard = new Discard(true);
		this.epidemicDiscard = new Discard(false);
	}
	
	/**
	 * Use to get all the Diseases of the map
	 * @return a tab of disease
	 */
	public Disease[] getDiseases() {
		return this.diseases;
	}
	
	/**
	 * Use to get the deck of player card
	 * @return DeckPlayer
	 */
	public Deck getDeckPlayer() {
		return this.deckPlayer;
	}
	
	/**
	 * Use to get the deck of infection card
	 * @return DeckInfection
	 */
	public Deck getDeckInfection() {
		return this.deckInfection;
	}
	
	/**
	 * 
	 * @return
	 */
	public Deck getDeckEpidemic() {
		return this.deckEpidemic;
	}
	
	/**
	 * 
	 * @return
	 */
	public Discard getNormalDiscard() {
		return this.normalDiscard;
	}
	
	/**
	 * 
	 * @return
	 */
	public Discard getInfectionDiscard() {
		return this.infectionDiscard;
	}
	
	/**
	 * 
	 * @return
	 */
	public Discard getEpidemicDiscard() {
		return this.epidemicDiscard;
	}
	
	/**
	 * get all towns on the game map
	 * @return all towns on map
	 */
	public List<Town> getTowns() {
		return this.towns;
	}
	
	/**
	 * Use to get the nb of town in the map
	 * @return the nb of town
	 */
	public int getNbOfTown() {
		return this.towns.size();
	}
	
		
	/**
	 * get the neighbors of a given town
	 * @param currentTown the given town
	 * @return a list of the neighbors of the given town
	 */
	public List<Town> getTownNeighbor(Town currentTown) {
		List<Town> townNeighbor = new ArrayList<>();
		for (Object o: this.reader.getNeighbors().getJSONArray(currentTown.getName())) {
			String townName = (String) o;
			for (Town town : this.getTowns()) {
				if (townName.equals(town.getName())) {
					townNeighbor.add(town);
				}
			}
		}
		return townNeighbor;
	}
	
	


	
}
