package virus;

import java.util.*;

public enum DiseaseName {
	  JAUNE , BLEU , ROUGE , NOIR;

	public static List<DiseaseName> valuesAsList () {
		return new ArrayList<DiseaseName>(Arrays.asList(DiseaseName.values()));
	}
}
