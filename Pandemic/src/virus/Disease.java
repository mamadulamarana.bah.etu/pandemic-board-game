package virus;



public class Disease {
	/** name of the virus*/
	private DiseaseName name ;
	
	/** the number of used cube */
	private int nbCube ;
	
	/** sector who implants the virus*/
	private int sector ;
	
	/** knowing if the disease is cured */
	private boolean cured;

	/** the number of remedy */
	private static int nb_Remedy;
	/** if a disease is eradicate or not*/
	private boolean eradicate;  		/** modif UML **/
		
	/** the total of cube  in game */
	private static  final int NB_CUBE_MAX= 24 ;
	
	/**
	 * Build a virus for the game, the virus propagate in the map and contaminate the cities
	 * @param sector use to know the sector's number
	 */
	public Disease(int sector ) {
		this.sector = sector;
		this.nbCube = Disease.NB_CUBE_MAX;
		this.cured = false;  		/**modif UML **/
		this.eradicate = false;
		Disease.nb_Remedy = 0;
	}
	
	/** 
	 * return the name of this disease
	 * @return the name of this disease
	 */
	public DiseaseName getName() {
		return DiseaseName.values()[this.sector];
	}
	
	/**
	 * Use to get the sector's number
	 * @return use to get the sector's number (int)
	 */
	public int getSector(){
		return this.sector;
		
	}
	
	/**
	 * Use to get the maximum cube's number of this city
	 * @return the maximum cube's number (int)
	 */
	public int getNbCubesMax() {
		return Disease.NB_CUBE_MAX;
	}
	
	/**
	 * return the numbers of cube of this disease
	 * @return the number of cube (int)
	 */
	public int getNbCube() {
		return this.nbCube;
	}
	
	/**
	 * Use to reset the  cube of this disease
	 * @param nbCube of this disease
	 */
	public void resetCube(int nbCube) {
		this.nbCube += nbCube;
		if (this.isCured() && this.nbCube == Disease.NB_CUBE_MAX) {
			this.setEradicate();
			System.out.println("la maladie "+ this + " a ete eradiquee");
		}
	}
	
	/**
	 * check if we can put a disease cube on a town.
	 * @return true if we can set a cube on a town, false or not
	 * @throws NotAvailableCubeException there are no more cube available
	 */
	public boolean CanSetCube() throws NotAvailableCubeException {
		if(this.nbCube > 0) {
			this.nbCube--;
			return true;
		}
		else {
			throw new NotAvailableCubeException("il n'ya plus de cube disponible pour la maladie "+ this);
		}
	}
	
	/**
	 * Use to set that this disease has been eradicated
	 */
	public void setEradicate() {
		this.eradicate = true;
	}

	/**
	 * Use to know if the disease is eradicate
	 * @return true if it's eradicate, false if not
	 */
	public boolean isEradicate() {
		return this.eradicate;
	}
	
	/**
	 * Use to know if the disease is cured
	 * @return true if it's cured, false if not
	 */
	public boolean isCured() {	/** modif UML**/
		return this.cured;
	}
	
	/**
	 * Use to set that this disease has been cured
	 */
	public void setCured() {	/** modif UML **/
		Disease.nb_Remedy++;
		this.cured = true;
	}
	
	/**
	 * Use to get the nb of remedy
	 * @return the nb of remedy
	 */
	static public int getNbRemedy() {
		return Disease.nb_Remedy;
	}

	/**
	 * Equal's method for disease
	 * @param o The disease to compare
	 * @return true if it's equals, false if not
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Disease)) {
			return false;
		}
		else {
			Disease other = (Disease)o;
			return this.getSector() == other.getSector();
		}
	}
	
	/**
	 * Use to get the name of the disease
	 */
	public String toString() {
		return ""+ this.getName();
	}

		
}