package virus;

public class NotAvailableCubeException extends Exception {

	public NotAvailableCubeException() {
		
	}
	
	public NotAvailableCubeException(String msg) {
		super(msg);
	}
}
