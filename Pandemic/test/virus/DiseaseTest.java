package virus;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiseaseTest {

	@Test
	public void getNametest() {
		Disease disease = new Disease(1);
		assertEquals(DiseaseName.BLEU,disease.getName());
	}

	@Test
	public void getSectortest() {
		Disease disease = new Disease(1);
		assertEquals(1,disease.getSector());
	}
	
	@Test
	public void getNbCubetest() {
		Disease disease = new Disease(1);
		assertEquals(24,disease.getNbCube());
	}
	
	@Test
	public void canSetCubetest() throws NotAvailableCubeException {
		Disease disease = new Disease(1);
		disease.CanSetCube();
		assertEquals(23,disease.getNbCube());
	}
	
	@Test (expected = NotAvailableCubeException.class)
	public void canSetCubetestButThrowException() throws NotAvailableCubeException {
		Disease disease = new Disease(1);
		while(true) {
			disease.CanSetCube();
		}
	}

	@Test
	public void isEradicateTest(){
		Disease disease = new Disease(1);
		disease.setEradicate();
		assertTrue(disease.isEradicate());
	}

	@Test
	public void isCuredTest(){
		Disease disease = new Disease(1);
		disease.setCured();
		assertTrue(disease.isCured());
	}
}
