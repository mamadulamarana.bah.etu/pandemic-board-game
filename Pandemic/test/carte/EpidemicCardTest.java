package carte;

import static org.junit.Assert.*;

import org.junit.Test;

public class EpidemicCardTest {

	@Test
	public void usetest() {
		EpidemicCard card = new EpidemicCard();
		assertFalse(card.use());
	}
	
	@Test
	public void isEpidemicTest(){
		EpidemicCard card = new EpidemicCard();
		assertTrue(card.isEpidemic());
	}
	
	@Test
	public void isInfectionTest() {
		EpidemicCard card = new EpidemicCard();
		assertFalse(card.isInfectionCard());
	}

}
