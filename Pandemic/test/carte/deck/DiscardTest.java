package carte.deck;

import static org.junit.Assert.*;

import org.junit.Test;

import carte.EpidemicCard;
import carte.NormalCard;
import carte.deck.Discard;
import town.Town;
import virus.Disease;

public class DiscardTest {

	@Test
	public void addCardtest() {
		Discard discard = new Discard();
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		discard.addCard(card);
		assertTrue(discard.getDiscard().contains(card));
	}

	@Test
	public void addCardEpidemictest() {
		Discard discard = new Discard(false);
		EpidemicCard card = new EpidemicCard();
		discard.addCard(card);
		assertFalse(discard.getDiscard().isEmpty());
	}
	
	@Test
	public void addInfectionCardTest() {
		Discard discard = new Discard(true);
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		card.setInfectionCard();
		discard.addCard(card);
		assertFalse(discard.getDiscard().isEmpty());
	}
	
	@Test
	public void isEpidemicDiscardTest(){
		Discard discard = new Discard(false);
		assertTrue(discard.isEpidemicDiscard());
	}
	
	
	@Test
	public void isInfectionDiscardTest(){
		Discard discard = new Discard(true);
		assertTrue(discard.isinfectionDiscard());
	}
	
	
}
