package carte;

import static org.junit.Assert.*;

import org.junit.Test;

import town.Town;
import virus.Disease;
import virus.DiseaseName;
import virus.NotAvailableCubeException;

public class NormalCardTest {

	@Test
	public void useIfInfectiontest() throws NotAvailableCubeException {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		card.setInfectionCard();
		assertFalse(card.use());
		assertEquals(town.getNbDisease(),1);
	}
	
	@Test
	public void useIfNotInfectiontest() throws NotAvailableCubeException {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		assertTrue(card.use());
	}
	
	@Test (expected = NotAvailableCubeException.class)
	public void useInfectionException() throws NotAvailableCubeException {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		card.setInfectionCard();
		while(true) {
			card.use();
		}
	}
	
	@Test
	public void isEpidemictest() {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		assertFalse(card.isEpidemic());
	}

	@Test
	public void getTowntest() {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		assertEquals(card.getTown(),town);
	}
	
	@Test
	public void getDiseasetest() {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		assertEquals(card.getDisease(),disease);
	}
	
	
	
}
