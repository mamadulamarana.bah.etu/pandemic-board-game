package town;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import virus.*;
import org.junit.Test;

import Player.Player;
public class TownTest {

	@Test
	public void TryToSetStationtest() {
		Town town = new Town("ville-1",1);
		town.setStation();
		assertTrue(town.isStation());
	}
	@Test
	public void TryToUnSetStationtest() {
		Town town = new Town("ville-1",1);
		town.setStation();
		town.removeStation();
		assertFalse(town.isStation());
	}
	
	@Test
	public void Infectiontest() throws NotAvailableCubeException{
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		town.infection(disease,1);
		assertEquals(town.getNbDisease(),1);
	}
	
	@Test
	public void OutOfBreaktest() throws NotAvailableCubeException{
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		town.infection(disease,1);
		town.infection(disease,1);
		town.infection(disease,1);
		town.infection(disease,1);
		assertTrue(town.isOutbreak());
	}
	
	@Test
	public void getNameTest() {
		Town town = new Town("ville-1",1);
		assertEquals(town.getName(),"ville-1");
	}
	
	@Test
	public void getNbDiseaseTest() throws NotAvailableCubeException {
		Town town = new Town("ville-1",1);
		Disease d1 = new Disease(1);
		Disease d2 = new Disease(0);
		town.infection(d1,1);
		town.infection(d2,1);
		assertEquals(2,town.getNbDisease());
	}

	@Test
	public void getDiseaseTest() throws NotAvailableCubeException {
		Town town = new Town("ville-1",1);
		Disease d1 = new Disease(1);
		town.infection(d1,1);
		assertTrue(town.getDiseases().containsKey(d1));
	}



	
	@Test
	public void getNeighborsTest() {
		Town town1 = new Town("ville-1",1);
		Town town2 = new Town("ville-2",1);
		Town town3 = new Town("ville-3",1);
		List<Town> towns = new ArrayList();
		towns.add(town2);
		towns.add(town3);
		town1.setNeighbors(towns);
		assertEquals(towns,town1.getNeighbors());
	}
	
	@Test
	public void getSectorTest() {
		Town town1 = new Town("ville-1",1);
		assertEquals(1,town1.getSector());
	}
	
	@Test
	public void propageInfectiontest() throws NotAvailableCubeException{
		Town town = new Town("ville-1",1);
		Town town2 = new Town("ville-2",1);
		Town town3 = new Town("ville-3",1);
		List<Town> towns = new ArrayList();
		towns.add(town2);
		towns.add(town3);
		town.setNeighbors(towns);
		Disease disease = new Disease(1);
		town.infection(disease,1);
		town.infection(disease,1);
		town.infection(disease,1);
		town.infection(disease,1);
		assertEquals(town2.getNbDisease(),1);
		assertEquals(town3.getNbDisease(),1);
	}

	@Test
	public void tryToSetPlayer(){
		Town town1 = new Town("ville-1",1);
		Player p = new Player("p");
		town1.setPlayer(p);
		assertTrue(town1.getPlayers().contains(p));
	}

	public void tryToRemovePLayer(){
		Town town1 = new Town("ville-1",1);
		Player p = new Player("p");
		town1.setPlayer(p);
		town1.removePlayer(p);
		assertFalse(town1.getPlayers().contains(p));

	}
}
