package Player.role;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import carte.NormalCard;
import map.ClassicalMappeTest;
import map.JSONReader;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
import Player.Player;


public class GlobeTrotteurTest {

    @Test
    public void moveTest(){
        Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role globe = new GlobeTrotteur("globe",null);
		roles.add(globe);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        player.setTown(town);
        globe.move(player);
        assertFalse(town.equals(player.getTown()));
    }
    /** Ne marche pas car impossible de créer une map car elle nécessite le json*/

}