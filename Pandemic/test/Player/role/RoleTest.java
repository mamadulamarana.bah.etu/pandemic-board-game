package Player.role;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import carte.NormalCard;
import carte.deck.Discard;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
import virus.NotAvailableCubeException;
import Player.Player;
import Player.PlayerCardUnavailableException;
import Player.StationUnavailableException;

public class RoleTest {
	
	/** Pas les testes de getMap, get TownWithStation, move et de build car la création de map nécessite un json qu'on arrive pas a importe dans les tests*/
	@Test
	public void getPlayerRoleTest(){
		Role role = new Role("role",null);
		assertEquals(role.getPlayerRole(),"role");
	}
	
	@Test
	public void getNbRequiredCardTest(){
		Role role = new Role("role",null);
		assertEquals(role.getNbRequiredCard(),5);
	}
	
	
	@Test
	public void setPlayerTest(){
		Role role = new Role("role",null);
		Player p = new Player("oui");
		role.setPlayer(p);
		}
	
	@Test (expected = StationAvailableException.class)
	public void buildTestStationAvailableException() throws PlayerCardUnavailableException, StationAvailableException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        town.setStation();
        player.setTown(town);
        role.build(player);
	}
	
	@Test (expected = PlayerCardUnavailableException.class)
	public void buildTestPlayerCardUnavailableException() throws PlayerCardUnavailableException, StationAvailableException {
		Discard discard = new Discard();
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        Town town1 = new Town("ville-1",1);
		Town town2 = new Town("ville-2",2);
		Disease disease = new Disease(1);
		Disease disease2 = new Disease(2);
		NormalCard card1 = new NormalCard(town1,disease);
		NormalCard card2 = new NormalCard(town2,disease2);
		player.addCard(card1,discard);
		player.addCard(card2,discard);
        player.setTown(town);
        role.build(player);
	}
	/*
	@Test
	public void buildTest() throws PlayerCardUnavailableException, StationAvailableException {
		Discard discard = new Discard();
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("expert",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
		Town town2 = new Town("ville-2",2);
		Disease disease = new Disease(1);
		Disease disease2 = new Disease(2);
		NormalCard card1 = new NormalCard(town,disease);
		NormalCard card2 = new NormalCard(town2,disease2);
		player.addCard(card1,discard);
		player.addCard(card2,discard);
        player.setTown(town);
        role.build(player);
        
	}
	 Ne marche pas car n'a pas accès à la défausse de la map
	*/
	
	/**
	@Test
    public void moveTest(){
        Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        player.setTown(town);
        role.move(player);
        assertFalse(town.equals(player.getTown()));
    }
	*/
	
	@Test (expected = StationUnavailableException.class)
	public void CureTestStationUnavailableException() throws PlayerCardUnavailableException, StationUnavailableException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        player.setTown(town);
        role.Cure(player);
	}
	
	@Test (expected = PlayerCardUnavailableException.class)
	public void CureTestPlayerCardUnavailableException() throws PlayerCardUnavailableException, StationUnavailableException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        town.setStation();
        player.setTown(town);
        role.Cure(player);
	}
	
	
	/**
	 @Test
	public void CureTest() throws PlayerCardUnavailableException, StationUnavailableException {
		Discard discard = new Discard();
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("expert",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        Town town2 = new Town("ville-2",1);
		Town town3 = new Town("ville-3",1);
		Town town4 = new Town("ville-4",1);
		Town town5 = new Town("ville-5",1);
		Town town6 = new Town("ville-6",1);
		Disease disease = new Disease(1);
		NormalCard card1 = new NormalCard(town,disease);
		NormalCard card2 = new NormalCard(town2,disease);
		NormalCard card3 = new NormalCard(town3,disease);
		NormalCard card4 = new NormalCard(town4,disease);
		NormalCard card5 = new NormalCard(town5,disease);
		NormalCard card6 = new NormalCard(town5,disease);
		player.addCard(card1,discard);
		player.addCard(card2,discard);
		player.addCard(card3,discard);
		player.addCard(card4,discard);
		player.addCard(card5,discard);
		player.addCard(card6,discard);
        town.setStation();
        player.setTown(town);
        role.Cure(player);
	}
	Impossible car accès à getNormalDiscard de map impossible
	*/
	
	@Test
	public void TreatTest() throws NotAvailableCubeException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Role role = new Role("role",null);
		roles.add(role);
		player.setRole(roles);
		Disease disease = new Disease(1);
        Town town = new Town("ville-1",1);
        town.infection(disease, 0);
        town.setStation();
        player.setTown(town);
        role.treat(player);
        assertEquals(town.getNbDisease(),0);
	}
	
	
	
	
	
	
	
	
}