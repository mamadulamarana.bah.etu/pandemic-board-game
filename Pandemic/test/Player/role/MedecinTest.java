package Player.role;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import carte.NormalCard;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
import virus.NotAvailableCubeException;
import Player.Player;

public class MedecinTest {
	
	@Test
	public void TreatTest() throws NotAvailableCubeException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Medecin role = new Medecin("Medecin",null);
		roles.add(role);
		player.setRole(roles);
		Disease disease = new Disease(1);
        Town town = new Town("ville-1",1);
        town.infection(disease, 4);
        int i = town.getDiseases().get(disease);
        assertEquals(i,4);
        town.setStation();
        player.setTown(town);
        role.treat(player);
        assertEquals(town.getNbDisease(),0);
	}
}