package Player.role;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import carte.NormalCard;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
import Player.Player;
import Player.PlayerCardUnavailableException;
import Player.StationUnavailableException;

public class ScientistTest {
	
	@Test (expected = StationUnavailableException.class)
	public void CureTestStationUnavailableException() throws PlayerCardUnavailableException, StationUnavailableException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
        Scientist role = new Scientist("scientist",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        player.setTown(town);
        role.Cure(player);
	}
	
	@Test (expected = PlayerCardUnavailableException.class)
	public void CureTestPlayerCardUnavailableException() throws PlayerCardUnavailableException, StationUnavailableException {
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
        Scientist role = new Scientist("Scientist",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        town.setStation();
        player.setTown(town);
        role.Cure(player);
	}
	
	/**
	 * @Test
	public void CureTest() throws PlayerCardUnavailableException, StationUnavailableException {
		Discard discard = new Discard();
		Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Scientist role = new Scientist("Scientist",null);
		roles.add(role);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        Town town2 = new Town("ville-2",1);
		Town town3 = new Town("ville-3",1);
		Town town4 = new Town("ville-4",1);
		Town town5 = new Town("ville-5",1);
		Town town6 = new Town("ville-6",1);
		Disease disease = new Disease(1);
		NormalCard card1 = new NormalCard(town,disease);
		NormalCard card2 = new NormalCard(town2,disease);
		NormalCard card3 = new NormalCard(town3,disease);
		NormalCard card4 = new NormalCard(town4,disease);
		NormalCard card5 = new NormalCard(town5,disease);
		NormalCard card6 = new NormalCard(town5,disease);
		player.addCard(card1,discard);
		player.addCard(card2,discard);
		player.addCard(card3,discard);
		player.addCard(card4,discard);
		player.addCard(card5,discard);
		player.addCard(card6,discard);
        town.setStation();
        player.setTown(town);
        role.Cure(player);
	}
	*/
}