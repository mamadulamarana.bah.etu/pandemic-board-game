package Player.role;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import carte.NormalCard;
import town.Town;
import virus.Disease;
import virus.DiseaseName;
import Player.Player;

public class ExpertTest {

    @Test (expected = StationAvailableException.class)
    public void buildTestException() throws StationAvailableException {
        Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Expert expert = new Expert("expert",null);
		roles.add(expert);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        town.setStation();
        player.setTown(town);
        expert.build(player);
    }


    @Test
    public void buildTestwhenSetStation() throws StationAvailableException {
        Player player = new Player("player");
        List<Role> roles = new ArrayList<>();
		Expert expert = new Expert("expert",null);
		roles.add(expert);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        player.setTown(town);
        expert.build(player);
        assertTrue(town.isStation());
    }

    /**
     *  Impossible à tester car expert.build() utilise getTowns de map dont il a pas accès.
    @Test
    public void buildTestwhenCantSetStation() throws StationAvailableException {
        Player player = new Player("player");
        List<Role> roles= new ArrayList<>();
		Expert expert = new Expert("expert",null);
		roles.add(expert);
		player.setRole(roles);
        Town town = new Town("ville-1",1);
        Town town2 = new Town("ville-2",1);
        Town town3 = new Town("ville-3",1);
        Town town4 = new Town("ville-4",1);
        Town town5 = new Town("ville-5",1);
        Town town6 = new Town("ville-6",1);
        Town town7 = new Town("ville-7",1);
        Town town8 = new Town("ville-8",1);
        player.setTown(town);
        town2.setStation();
        town3.setStation();
        town4.setStation();
        town5.setStation();
        town6.setStation();
        town7.setStation();
        town8.setStation();
        expert.build(player);
        assertTrue(town.isStation());
        assertTrue(town2.isStation());
        assertTrue(town3.isStation());
        assertTrue(town4.isStation());
        assertTrue(town5.isStation());
        assertTrue(town6.isStation());
        assertTrue(town7.isStation());
        assertTrue(town8.isStation());
    }
	*/

}