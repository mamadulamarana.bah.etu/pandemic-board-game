package Player;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

import org.junit.Test;

import Player.role.*;
import carte.*;
import carte.deck.Deck;
import carte.deck.Discard;
import map.ClassicalMappe;
import map.JSONReader;
import map.Mappe;
import town.Town;
import virus.Disease;
import virus.DiseaseName;

public class PlayerTest {
	/** Les tests de takeCardTest et takeCardTestException sont commenté car on ne peut pas créer de deck sans map et on peut pas pas créer de map sans json ce que l'on n'arrive pas a import*/
	
	private static final String json = "ville.json";
	@Test
	public void getNametest() {
		Player joueur = new	Player("Joe");
		assertEquals("Joe",joueur.getName());
	}
	
	@Test
	public void setTowntest() {
		Town town = new Town("ville-1",1);
		Player joueur = new	Player("Joe");
		joueur.setTown(town);
		assertEquals(town,joueur.getTown());
	}
	
	@Test
	public void addCardtest() {
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		Discard discard = new Discard();
		Player joueur = new	Player("Joe");
		joueur.addCard(card,discard);
		assertEquals(card,joueur.getCard(0));
	}
	
	@Test
	public void addCardWhenMainFulltest() {
		Discard discard = new Discard();
		Town town1 = new Town("ville-1",1);
		Town town2 = new Town("ville-2",1);
		Town town3 = new Town("ville-3",1);
		Town town4 = new Town("ville-4",1);
		Town town5 = new Town("ville-5",1);
		Town town6 = new Town("ville-6",1);
		Town town7 = new Town("ville-7",1);
		Town town8 = new Town("ville-8",1);
		Disease disease = new Disease(1);
		NormalCard card1 = new NormalCard(town1,disease);
		NormalCard card2 = new NormalCard(town2,disease);
		NormalCard card3 = new NormalCard(town3,disease);
		NormalCard card4 = new NormalCard(town4,disease);
		NormalCard card5 = new NormalCard(town5,disease);
		NormalCard card6 = new NormalCard(town6,disease);
		NormalCard card7 = new NormalCard(town7,disease);
		NormalCard card8 = new NormalCard(town8,disease);
		Player joueur = new	Player("Joe");
		joueur.addCard(card1,discard);
		joueur.addCard(card2,discard);
		joueur.addCard(card3,discard);
		joueur.addCard(card4,discard);
		joueur.addCard(card5,discard);
		joueur.addCard(card6,discard);
		joueur.addCard(card7,discard);
		joueur.addCard(card8,discard);
		assertEquals(7,joueur.getMain().size());
	}
	
	
	@Test
	public void delCardtest() throws IndexOutOfBoundsException,UnsupportedOperationException {
		Discard discard = new Discard();
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		Player joueur = new	Player("Joe");
		joueur.addCard(card,discard);
		assertEquals(card,joueur.delCard(0));
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void delCardwithErrortest() throws IndexOutOfBoundsException,UnsupportedOperationException {
		Discard discard = new Discard();
		Town town = new Town("ville-1",1);
		Disease disease = new Disease(1);
		NormalCard card = new NormalCard(town,disease);
		Player joueur = new	Player("Joe");
		joueur.addCard(card,discard);
		joueur.delCard(8);
	}
	
	@Test
	public void getRoleTest(){
		Player player = new Player("Joe");
		List<Role> roles = new ArrayList<>();
		Role expert = new Expert("expert",null);
		roles.add(expert);
		player.setRole(roles);
		assertEquals(expert,player.getRole());
	}

	@Test
	public void getMainTest(){
		Discard discard = new Discard();
		Town town1 = new Town("ville-1",1);
		Town town2 = new Town("ville-2",1);
		Disease disease = new Disease(1);
		NormalCard card1 = new NormalCard(town1,disease);
		NormalCard card2 = new NormalCard(town2,disease);
		Player joueur = new	Player("Joe");
		joueur.addCard(card1,discard);
		joueur.addCard(card2,discard);
		assertTrue(joueur.getMain().contains(card1));
		assertTrue(joueur.getMain().contains(card2));
	}

	@Test
	public void getNbActionTest(){
		Player joueur = new	Player("Joe");
		joueur.increaseNbAction();
		assertEquals(1,joueur.getNbAction());
	}

	@Test
	public void resetNbActionTest(){
		Player joueur = new	Player("Joe");
		joueur.increaseNbAction();
		joueur.increaseNbAction();
		joueur.resetNbAction();
		assertEquals(0,joueur.getNbAction());
	}
	
	/**
	@Test
	public void takeCardtest() throws EmptyStackException{
		Mappe map = new ClassicalMappe(new JSONReader(json));
		Deck deck = new Deck(null);
		Player joueur = new	Player("Joe");
		Card card = deck.getStack().peek();
		assertEquals(card,joueur.takeCard(deck));
	}

	@Test (expected = EmptyStackException.class)
	public void takeCardtestException() throws EmptyStackException{
		Mappe map = new ClassicalMappe(new JSONReader(json));
		Deck deck = new Deck(null);
		Player joueur = new	Player("Joe");
		while(true){
			joueur.takeCard(deck);
		}
	}
	*/
	
	@Test
	public void getDiseasesOnHisHandTest(){
		Discard discard = new Discard();
		Player joueur = new	Player("Joe");
		Town town1 = new Town("ville-1",1);
		Town town2 = new Town("ville-2",2);
		Disease disease = new Disease(1);
		Disease disease2 = new Disease(2);
		NormalCard card1 = new NormalCard(town1,disease);
		NormalCard card2 = new NormalCard(town2,disease2);
		joueur.addCard(card1,discard);
		joueur.addCard(card2,discard);
		assertTrue(joueur.getDiseasesOnHisHand().contains(disease));
		assertTrue(joueur.getDiseasesOnHisHand().contains(disease2));
	}


// Test de Play ????
}
